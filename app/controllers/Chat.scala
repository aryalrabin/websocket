package controllers

import play.api._
import libs.concurrent.Promise
import play.api.libs.iteratee.Concurrent.Channel
import play.api.libs.iteratee.{Concurrent, Enumerator, Iteratee}
import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by raryal on 4/7/2015.
 */
object Chat extends Controller {

  def index = Action{
    Ok(views.html.chat.index("aaa"))
  }

  var roomConnections : List[Channel[String]] = Nil

  def chat =  WebSocket.using[String] {
    request => {

      val (out,channel) = Concurrent.broadcast[String]
      // register
      roomConnections ::= channel

      val in = Iteratee.foreach[String](s => {
        println(s)
        val newRoomConnections = roomConnections.filterNot( _ == channel)
        // push message to each connections
        newRoomConnections.foreach(_.push("Received : " + s))

        val ownConnections = roomConnections.filter( _ == channel)
        // push message to each connections
        ownConnections.foreach(_.push("Sent : " + s))

      }).map(_ => {
        // unregister
        roomConnections = roomConnections.filterNot( _ == channel)
        println("Disconnected")
      })

      (in,out)

    }
  }

}
