package controllers

import play.api.libs.iteratee.{Iteratee, Concurrent}
import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global

object Application extends Controller {

  def index = Action {
    Ok(views.html.index("Echo.."))
  }

  def echo = WebSocket.using[String] {
    request => {
      // create PushEnumerator
      val (out,channel) = Concurrent.broadcast[String]
      val in = Iteratee.foreach[String](text => {
        println(text)
        // push
        channel.push(text)
      }).map(_ => {
        // on connection disconnected
        println("Disconnected")
      })
      in -> out
    }
  }


}